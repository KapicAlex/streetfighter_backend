const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query

  if (res.err) {
    let statusError = res.err?.status ?? 400;
    res.status(statusError).send({
      error: true,
      message: res.err?.message ?? "Something went wrong",
    });
  } else {
    res.status(200).send(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
