const { user } = require("../models/user");
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  const {
    firstName = "",
    lastName = "",
    email = "",
    phoneNumber = "",
    password = "",
  } = req.body;

  if (!firstName || !lastName || !email || !phoneNumber || !password) {
    res.status(400).send({
      error: true,
      message: "User entity has empty field",
    });
    throw new Error("All fields must be filled");
  } else if (!validName(firstName)) {
    res.status(400).send({
      error: true,
      message: "Firstname is not valid",
    });
    throw new Error(
      "First Name must be in english and at least contain two chars."
    );
  } else if (!validName(lastName)) {
    res.status(400).send({
      error: true,
      message: "Lastname is not valid",
    });
    throw new Error(
      "Last Name must be in english and at least contain two chars."
    );
  } else if (!validEmail(email)) {
    res.status(400).send({
      error: true,
      message: "Email is not valid",
    });
    throw new Error("Email must be in format *****@gmail.com");
  } else if (existingEmail(email)) {
    res.status(400).send({
      error: true,
      message: "Existing email",
    });
    throw new Error("Your email is already registered. Push 'sign in'.");
  } else if (existingPhone(phoneNumber)) {
    res.status(400).send({
      error: true,
      message: "Existing phoneNumber",
    });
    throw new Error("Your phone number is already registered. Push 'sign in'.");
  } else if (!validPhoneNumber(phoneNumber)) {
    res.status(400).send({
      error: true,
      message: "PhoneNumber is not valid",
    });
    throw new Error("Your phone number must be in format +380xxxxxxxxx");
  } else if (!validPass(password)) {
    res.status(400).send({
      error: true,
      message: "Password is not valid",
    });
    throw new Error(
      "Your passwor length must be at least 3 and don't contain any spaces."
    );
  } else if (Boolean(req.body.id)) {
    res.status(400).send({
      error: true,
      message: "Id in the body",
    });
    throw new Error("Wrong request to server");
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  const { email = "", phoneNumber = "", password = "" } = req.body;

  if (!validEmail(email)) {
    res.status(400).send({
      error: true,
      message: "Email is not valid",
    });
    throw new Error("Email must be in format *****@gmail.com");
  } else if (existingProperty(email)) {
    res.status(400).send({
      error: true,
      message: "Existing email",
    });
    throw new Error("Your email is already registered. Push 'sign in'.");
  } else if (existingProperty(phoneNumber)) {
    res.status(400).send({
      error: true,
      message: "Existing phoneNumber",
    });
    throw new Error("Your phone number is already registered. Push 'sign in'.");
  } else if (!validPhoneNumber(phoneNumber)) {
    res.status(400).send({
      error: true,
      message: "PhoneNumber is not valid",
    });
    throw new Error("Your phone number must be in format +380xxxxxxxxx");
  } else if (!validPass(password)) {
    res.status(400).send({
      error: true,
      message: "Password is not valid",
    });
    throw new Error(
      "Your passwor length must be at least 3 and don't contain any spaces."
    );
  } else if (Boolean(req.body.id)) {
    res.status(400).send({
      error: true,
      message: "Id in the body",
    });
    throw new Error("Wrong request to server");
  }

  next();
};

const validName = (name) => {
  for (let i = 0; i < name.length; i++) {
    if (
      name.toLowerCase().charCodeAt(i) < 97 ||
      name.toLowerCase().charCodeAt(i) > 122 ||
      name.length < 2 ||
      name.length > 20
    ) {
      return false;
    }
    return true;
  }
};

const validEmail = (email) => {
  const regEmail = /^((\.)?\w){2,63}@gmail\.com$/;
  return regEmail.test(email);
};

const validPhoneNumber = (phoneNumber) => {
  const regPhone = /^\+380\d{9}$/;
  return regPhone.test(phoneNumber);
};

const validPass = (password) => {
  return (
    password.replaceAll(" ", "").length > 2 &&
    password.replaceAll(" ", "").length === password.length
  );
};

const existingEmail = (email) => {
  if (UserService.search({ email })) {
    return true;
  }
  return false;
};
const existingPhone = (phoneNumber) => {
  if (UserService.search({ phoneNumber })) {
    return true;
  }
  return false;
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
