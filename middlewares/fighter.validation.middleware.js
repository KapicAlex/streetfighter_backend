const { fighter } = require("../models/fighter");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation

  const { name = "", health = 100, power = "", defense = "" } = req.body;

  if (!name || !power || !defense) {
    res.status(400).send({
      error: true,
      message: "Fighter entity has empty field",
    });
    throw new Error("All fields for fighter must be filled");
  } else if (!validName(name)) {
    res.status(400).send({
      error: true,
      message: "Fighter name is not valid",
    });
    throw new Error(
      "Fighter name must be in english and at least contain two chars."
    );
  } else if (existingName(name)) {
    res.status(400).send({
      error: true,
      message: "Existing fighter name",
    });
    throw new Error("Fighter name is already registered. Set another.");
  } else if (!validPower(power)) {
    res.status(400).send({
      error: true,
      message: "Power is not valid",
    });
    throw new Error("Power must be a number in the range from 2 to 99");
  } else if (!validDefense(defense)) {
    res.status(400).send({
      error: true,
      message: "Defense is not valid",
    });
    throw new Error("Defense must be a number in the range from 2 to 9");
  } else if (Boolean(req.body.id)) {
    res.status(400).send({
      error: true,
      message: "Id in the body",
    });
    throw new Error("Wrong request to server");
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update

  const { health = 100, power = "", defense = "" } = req.body;

  if (power && !validPower(!power)) {
    res.status(400).send({
      error: true,
      message: "Power is not valid",
    });
    throw new Error("Power must be a number in the range from 2 to 99");
  } else if (defense && !validDefense(defense)) {
    res.status(400).send({
      error: true,
      message: "Defense is not valid",
    });
    throw new Error("Defense must be a number in the range from 2 to 9");
  } else if (Boolean(req.body.id)) {
    res.status(400).send({
      error: true,
      message: "Id in the body",
    });
    throw new Error("Wrong request to server");
  }

  next();
};

const validName = (name) => {
  for (let i = 0; i < name.length; i++) {
    if (
      name.toLowerCase().charCodeAt(i) < 97 ||
      name.toLowerCase().charCodeAt(i) > 122 ||
      name.length < 2 ||
      name.length > 20
    ) {
      return false;
    }
    return true;
  }
};

const existingName = (name) => {
  if (FighterService.search({ name })) {
    return true;
  }
  return false;
};

const validPower = (power) =>
  power > 1 && power < 100 && !isNaN(parseInt(power));

const validDefense = (defense) =>
  defense > 1 && defense < 10 && !isNaN(parseInt(defense));

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
