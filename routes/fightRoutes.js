const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', (req, res, next) => {
  try {
      const logs = FightService.getAllLogs();
      res.data = logs;
  } catch (error) {
      res.err = error;
  } finally {
      next()
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
      const fight = FightService.getFight(req.params.id);
      res.data = fight;
  } catch (error) {
      res.err = error;
  } finally {
      next()
  }
}, responseMiddleware);

router.post('/', (req, res, next) => {
  try {
      const log = FightService.createLog(req.body);
      res.data = log
  } catch (error) {
      res.err = error
  } finally {
      next();
  }
}, responseMiddleware);


router.delete('/:id', (req, res, next) => {
  try {
      const deletedLog = FightService.delete(req.params.id);
      res.data = deletedLog;
  } catch (error) {
      res.err = error;
  } finally {
      next();
  }
}, responseMiddleware);

module.exports = router;