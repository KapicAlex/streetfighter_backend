import { get, post } from "../requestHelper";

export const getlog = async () => {
    return await get('fights');
}

export const sendLog = async (body) => {
    return await post('fights', body);
}
