const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const item = UserRepository.create(data);
    if (!item) {
      throw new Error("Can't create new User");
    }
    return item;
  }

  getAll() {
    const item = UserRepository.getAll();
    if (!item.length) {
      throw new Error("Users not found");
    }
    return item;
  }

  getOne(id) {
    const item = UserRepository.getOne({ id });
    if (!item) {
      throw new Error("User not found");
    }
    return item;
  }

  update(id, dataToUpdate) {
    const item = UserRepository.update(id, dataToUpdate);
    if (!item.id) {
      throw new Error("Can't update User, not found");
    }
    return item;
  }

  delete(id) {
    const item = UserRepository.delete(id);
    if (!item.length) {
      throw new Error("User not found");
    }
    return item;
  }
}

module.exports = new UserService();
