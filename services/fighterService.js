const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const item = FighterRepository.create(data);
    if (!item) {
      throw new Error("Can't create new Fighter");
    }
    return item;
  }

  getAll() {
    const item = FighterRepository.getAll();
    if (!item.length) {
      throw new Error("Fighters not found");
    }
    return item;
  }

  getOne(id) {
    const item = FighterRepository.getOne({ id });
    if (!item) {
      throw new Error("Fighter not found");
    }
    return item;
  }

  update(id, dataToUpdate) {
    const item = FighterRepository.update(id, dataToUpdate);
    if (!item.id) {
      throw new Error("Can't update Fighter, not found");
    }
    return item;
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item.length) {
      throw new Error("Fighter not found");
    }
    return item;
  }
}

module.exports = new FighterService();
