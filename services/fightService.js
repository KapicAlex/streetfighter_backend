const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  // OPTIONAL TODO: Implement methods to work with fights

  createLog(data) {
    const log = FightRepository.create(data);
    if (!log) {
      throw new Error("Can't save fight log");
    }
    return log;
  }

  getAllLogs() {
    const logs = FightRepository.getAll();
    if (!logs.length) {
      throw new Error("Fights in log not found");
    }
    return log;
  }

  getFight(id) {
    const log = FightRepository.getOne({ id });
    if (!log) {
      throw new Error("Fight not found");
    }
    return log;
  }

  delete(id) {
    const log = FightRepository.delete(id);
    if (!item.length) {
      throw new Error("Can't delete fight log");
    }
    return log;
  }
}

module.exports = new FightersService();
